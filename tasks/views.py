from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)  # ??html


@login_required
def show_my_tasks(request):
    tasks_object_detail = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_object_detail": tasks_object_detail,
    }
    return render(request, "tasks/mine.html", context)
